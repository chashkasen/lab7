#pragma once
#define N 100
#include<math.h>
#include <iostream>

namespace km
{
	void Read(int& n, int mas[N][N]);

	bool Stolb(int n, int mas[N][N]);

	bool IsPrime(int x);

	bool Elements(int n, int mas[N][N]);

	int Module(int x);

	int SumString(int i, int n, int mas[N][N]);

	void Sort(int n, int mas[N][N]);

	void Write(int n, int mas[N][N]);
}