#include <Functions.hpp>


namespace km
{

	void Read(int& n, int mas[N][N])
	{
		std::cin >> n;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				std::cin >> mas[i][j];
	}

	bool Stolb(int n, int mas[N][N])
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				if (mas[i][j] != mas[i][j + 1])
					return false;
		return true;
	}

	bool IsPrime(int x)
	{
		if (x < 2)
			return false;
		for (int d = 2; d <= sqrt(x); d++)
			if (x % d == 0)
				return false;
		return true;

	}

	bool Elements(int n, int mas[N][N])
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				if (IsPrime(mas[i][j]))
					return false;
		return true;
	}

	int Module(int x)
	{
		if (x < 0)
			x = 0 - x;
		return x;
	}

	int SumString(int i, int n, int mas[N][N])
	{
		int k = 0;
		for (int j = 0; j < n; j++)
			k += Module(mas[i][j]);
		return k;

	}

	void Sort(int n, int mas[N][N])
	{
		for (int i = 0; i < n - 1; i++)
			if (SumString(i, n, mas) > SumString(i + 1, n, mas))
				for (int j = 0; j < n; j++)
					std::swap(mas[i][j], mas[i + 1][j]);
	}

	void Write(int n, int mas[N][N])
	{
		for (int i = 0; i < n; i++)
		{
			std::cout << std::endl;
			for (int j = 0; j < n; j++)
				std::cout << mas[i][j] << " ";
		}
	}
}
